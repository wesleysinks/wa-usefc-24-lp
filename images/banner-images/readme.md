 # Convert Large Images to WEBP and Scale to max 2000x2000px

 ```bash
 mogrify -resize 2000x2000 -format webp -quality 80 *.jpg
 ```